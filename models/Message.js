const mongoose = require('mongoose');

const MessageSchema = new mongoose.Schema({
    messageId: Schema.Types.ObjectId,
    chatId: Schema.Types.ObjectId,
    createdAt: {type: Date, default: Date.now},
    senderId: Schema.Types.ObjectId,
    Content: Mixed,
    readUserId: [Schema.Types.ObjectId]
});

module.exports = mongoose.model('Message', MessageSchema);
