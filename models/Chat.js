const mongoose = require('mongoose');

const ChatSchema = new mongoose.Schema({
    chatId: Schema.Types.ObjectId,
    userList: [Schema.Types.ObjectId],
    ChatLastMessageId: Schema.Types.ObjectId,
    chatTitle: String,
});

module.exports = mongoose.model('Chat', ChatSchema);
