const mongoose = require('mongoose');

const GameSchema = new mongoose.Schema({
    gameId: Schema.Types.ObjectId,
    createdAt: {type: Date, default: Date.now},
    playersId: [Schema.Types.ObjectId],
    lastPlayerMoveId: Schema.Types.ObjectId,
    board: Mixed,
});

module.exports = mongoose.model('Game', GameSchema);
